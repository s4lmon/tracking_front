FROM gtrafimenkov/ubuntu1604-openjdk-sbt
RUN apt-get update  && apt-get install -yq curl && cd /tmp && \
curl -sL https://deb.nodesource.com/setup_7.x | bash && apt-get install -yq nodejs \
 && curl -o- -L https://yarnpkg.com/install.sh | bash; exit 0
EXPOSE 9000
WORKDIR /app
#RUN bash yarn install
COPY . /app/.
RUN cd /app && chmod +x shell.sh  && chmod +x activator \
 && /root/.yarn/bin/yarn install >> /dev/null && /root/.yarn/bin/yarn add webpack >> /dev/null &&  /root/.yarn/bin/yarn webpack; exit 0
# After - will stay alive
RUN   echo 'while [ true ]; do sleep 1; done' > /keep-alive && \
  chmod +x /keep-alive
EXPOSE 9000 8888
ENTRYPOINT cd /app && /bin/bash -c sh ./activator run