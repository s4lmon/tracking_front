package controllers

import javax.inject._
import play.api._
import play.api.mvc._

/**
  * This controller creates an `Action` to handle HTTP requests to the
  * application's home page.
  */

case class Credentials(email: String, password: String)


@Singleton
class LoginController @Inject() extends Controller {

  /**
    * Create an Action to render an HTML page.
    *
    * The configuration in the `routes` file means that this method
    * will be called when the application receives a `GET` request with
    * a path of `/`.
    */
  def get = Action { implicit request =>
    println("///////")
    Ok(views.html.login())
  }

  def post = Action(parse.multipartFormData) {
    request =>
      println("/------")
      val formData = request.body.asFormUrlEncoded
      val email: String = formData("email").head
      val password: String = formData("password").head
      val credentials = Credentials(email, password)

      println(email, password)

      Ok("holaaa")
  }


}
