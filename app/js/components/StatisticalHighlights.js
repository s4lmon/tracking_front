import React from 'react';
import PropTypes from 'prop-types';
import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend, Cell } from 'recharts';

const Label = (props) => {
    console.log(props)
    return <text dominantBaseline="inherit"
        fill="#4a4a4a" width={props.width}
        height={props.height} x={props.x}
        y={props.y} cursor="pointer"
        className="recharts-text recharts-bar-label"
        textAnchor="middle">
        <tspan x={props.x} dy="0em">{props.count} Viajes</tspan>
    </text>
}

class StatisticalHighlights extends React.Component {

    render() {
        const data = [
            { name: 'Atrasos', count: this.props.delayCount },
            { name: 'Adelantados', count: this.props.aheadsCount },
            { name: 'A tiempo', count: this.props.onTimeCount }
        ];
        let width = (30 * document.getElementsByTagName("body")[0].clientWidth / 100);
        return <div className="highlight">
            <div className="col xl7 l7 m7 s7">
                <div className="col xl4 l4 m4 s4">
                    <p className='margin-bottom-0 left-align'>Total de atrasos</p>
                    <p className='font-big no-margin left-align'>{this.props.totalDelaysMinutes}<span className='font-min'>Min</span></p>
                </div>
                <div className="col xl4 l4 m4 s4">
                    <p className='margin-bottom-0 left-align'>Atrazo máximo</p>
                    <p className='font-big no-margin left-align'>{this.props.maxDelayMinutes}<span className='font-min'>Min</span></p>
                </div>
                <div className="col xl4 l4 m4 s4">
                    <p className='margin-bottom-0 left-align'>Promedio de atrasos</p>
                    <p className='font-big no-margin left-align'>{this.props.averageDelayMinutes}<span className='font-min'>Min</span></p>
                </div>
            </div>
            <div className="col xl5 l5 m5 s5">
                <BarChart width={width} height={110} data={data}>
                    <Bar dataKey="count" fill="#82ca9d" label={<Label />}>
                        {
                            data.map((entry, index) => {
                                return <Cell cursor="pointer"
                                    fill={entry.name === "Atrasos" ? '#f84e63' : entry.name === "Adelantados" ? '#7ed321' : '#ecedf0'}
                                    key={`cell-${index}`} />
                            })
                        }
                    </Bar>
                    <XAxis dataKey="name" />
                </BarChart>
            </div>
        </div>
    }
}



StatisticalHighlights.propTypes = {
    totalDelaysMinutes: PropTypes.number.isRequired,
    maxDelayMinutes: PropTypes.number.isRequired,
    averageDelayMinutes: PropTypes.number.isRequired,
    delayCount: PropTypes.number.isRequired,
    onTimeCount: PropTypes.number.isRequired,
    aheadsCount: PropTypes.number.isRequired
};



export default StatisticalHighlights;