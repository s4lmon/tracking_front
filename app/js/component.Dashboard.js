import React, { Component } from 'react';
//import {Table, Column, Cell} from 'fixed-data-table';
import StatisticalHighlights from './components/StatisticalHighlights'
var dateFormat = require('dateformat');
// Table data as a list of array.
const rows = [
    ['a1', 'b1', 'c1'],
    ['a2', 'b2', 'c2'],
    ['a3', 'b3', 'c3'],
    // .... and more
];

const routes = [
    {
        name: "Todas",
        stops: ["--", "--", "--", "--"]
    },

    {
        name: "Mena del Hierro - La Y",
        stops: ["Centro de Salud", "San Carlos Mena", "Fin Canal 4"]
    },
    {
        name: "La Loma - Magdalena",
        stops: ["San Carlos Loma", "Bosque Loma", "Comuna Loma", "Fin Loma"]
    },
    {
        name: "La Liga - Magdalena",
        stops: ["Entrada Roldos", "Bosque Liga", "Comuna Liga", "Fin Liga"]
    }

]

const drivers = [
    {
        name: "Cristian Ramon",
        device: "RM1823"
    },
    {
        name: "Marcelo Bonilla",
        device: "RM1254"
    },
    {
        name: "Victor Martinez",
        device: "RM8723"
    },
    {
        name: "Xavier Ramirez",
        device: "RM8723"
    },
    {
        name: "Chris Cornell",
        device: "RM1276"
    },
    {
        name: "Kurt Cobain",
        device: "RM9834"
    },
    {
        name: "Eddie Vedder",
        device: "RM9666"
    }
]


const chart = <StatisticalHighlights
    totalDelaysMinutes={2500}
    maxDelayMinutes={900}
    averageDelayMinutes={25.4}
    delayCount={20}
    onTimeCount={5}
    aheadsCount={11}
/>

class Dashboard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            login: false,
            error: false,
            checked: false,
            activeTab: 1,
            route: "Todas",
            stops: ["--", "--", "--", "--"],
            device: "",
            from: null,
            to: null,
            orderBy: "date",
            page: 1,
            data: [],
            isOpenedFilterDate: false,
            isOpenedFilterRoutes: false
        }
        this.onClickLogin = this.onClickLogin.bind(this);
        this.checkRememberPassword = this.checkRememberPassword.bind(this);
        this.menuItemClick = this.menuItemClick.bind(this);
        this.calculateContentHeight = this.calculateContentHeight.bind(this);
        this.onScroll = this.onScroll.bind(this);
        this.resetData = this.resetData.bind(this);
        this.paginateData = this.paginateData.bind(this);
        this.toggleDateFilter = this.toggleDateFilter.bind(this);
        this.handleAliasFilterChange = this.handleAliasFilterChange.bind(this)
        this.handleAliasFilterKeyPress = this.handleAliasFilterKeyPress.bind(this)
        this.dateHandleAliasFilterKeyPress = this.dateHandleAliasFilterKeyPress.bind(this)
        this.setDateFilter = this.setDateFilter.bind(this)
        this.openRoutesModal = this.openRoutesModal.bind(this)
        this.setsetRouteFilter = this.setsetRouteFilter.bind(this)
    }
    componentDidMount() {
        setTimeout(() => {
            this.calculateContentHeight()
            this.resetData()
        }, 1000);
    }

    openRoutesModal(){
        this.setState({ isOpenedFilterRoutes: !this.state.isOpenedFilterRoutes })
    }

    onScroll() {

    }

    setsetRouteFilter(id){
        var route = routes[id];
        this.setState({ route: route.name, stops: route.stops,  isOpenedFilterRoutes: false});
         setTimeout(() => {
            this.resetData()
        }, 1000);
    }

    setDateFilter(option) {
        var today = new Date()
        switch (option) {
            case "today":
                this.setState({to: Date.now(), from: Date.now(), isOpenedFilterDate: false});
                document.getElementById("lastWeek").classList.remove("active");
                document.getElementById("lastMonth").classList.remove("active");
                document.getElementById("today").classList.add("active");
                setTimeout(() => {
                    this.resetData()
                }, 500);
                break;
            case "lastWeek":
                this.setState({to: Date.now(), from: Date.now()-7*24*60*60000, isOpenedFilterDate: false});
                document.getElementById("lastMonth").classList.remove("active");
                document.getElementById("today").classList.remove("active");
                document.getElementById("lastWeek").classList.add("active");
                setTimeout(() => {
                    this.resetData()
                }, 500);
                break;
            case "lastMonth":
                this.setState({to: Date.now(), from: Date.now()-30*24*60*60000, isOpenedFilterDate: false});
                document.getElementById("lastWeek").classList.remove("active");
                document.getElementById("today").classList.remove("active");
                document.getElementById("lastMonth").classList.add("active");
                setTimeout(() => {
                    this.resetData()
                }, 500);
                break;
        }
    }

    handleAliasFilterKeyPress(event) {
        if (event.key == 'Enter') {
            this.resetData()
        }
    }

    dateHandleAliasFilterKeyPress(){
        this.setState({isOpenedFilterDate: false});
        setTimeout(() => {
            this.resetData()
        }, 500);
    }

    toggleDateFilter() {
        this.setState({ isOpenedFilterDate: !this.state.isOpenedFilterDate })
    }


    paginateData() {
        function getRandomInt(min, max) {
            return Math.floor(Math.random() * (max - min + 1)) + min;
        }

        function getDriver(device) {

            if (device === "") {
                return drivers[Math.floor(Math.random() * drivers.length)]
            } else {
                return drivers.find(item => item.device == device)
            }
        }

        let from = Date.now() - 24 * 60 * 60000
        if (this.state.from !== null) {
            from = this.state.from - 24 * 60 * 60000
        }
        let to = Date.now()
        if (this.state.to !== null) {
            to = this.state.to
        }
        let max = to


        let data = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
            1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
            1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
            1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
            1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10].map(x => {
                let driver = getDriver(this.state.device)
                let date = new Date(getRandomInt(from, max));
                let r1 = Math.floor(Math.random() * (40)) - 20;
                let d1 = new Date(date.getTime() + r1 * 60000 + 30 * 60000);
                let r2 = Math.floor(Math.random() * (40)) - 20;
                let d2 = new Date(d1.getTime() + r2 * 60000 + 30 * 60000);
                let r3 = Math.floor(Math.random() * (40)) - 20;
                let d3 = new Date(d2.getTime() + r3 * 60000 + 30 * 60000);
                let r4 = Math.floor(Math.random() * (40)) - 20;
                let d4 = new Date(d3.getTime() + r4 * 60000 + 30 * 60000);
                let diff_go = r1 + r2 + r3 + r4;
                let r5 = Math.floor(Math.random() * (40)) - 20;
                let d5 = new Date(d4.getTime() + r5 * 60000 + 30 * 60000);
                let r6 = Math.floor(Math.random() * (40)) - 20;
                let d6 = new Date(d5.getTime() + r6 * 60000 + 30 * 60000);
                let r7 = Math.floor(Math.random() * (40)) - 20;
                let d7 = new Date(d6.getTime() + r7 * 60000 + 30 * 60000);
                let r8 = Math.floor(Math.random() * (40)) - 20;
                let d8 = new Date(d7.getTime() + r8 * 60000 + 30 * 60000);
                let diff_back = r5 + r6 + r7 + r8;



                var hours = date.getHours();
                // Minutes part from the timestamp
                var minutes = "0" + date.getMinutes();



                return {
                    date: date,
                    device: driver == undefined ? "" : driver.device,
                    driverName: driver == undefined ? "" : driver.name,
                    stops_go: [
                        { from: dateFormat(date, "h:MM"), to: dateFormat(d1, "h:MM"), mins: r1 },
                        { from: dateFormat(d1, "h:MM"), to: dateFormat(d2, "h:MM"), mins: r2 },
                        { from: dateFormat(d2, "h:MM"), to: dateFormat(d3, "h:MM"), mins: r3 },
                        { from: dateFormat(d3, "h:MM"), to: dateFormat(d4, "h:MM"), mins: r4 },
                    ],
                    stops_back: [
                        { from: dateFormat(d4, "h:MM"), to: dateFormat(d5, "h:MM"), mins: r5 },
                        { from: dateFormat(d5, "h:MM"), to: dateFormat(d6, "h:MM"), mins: r6 },
                        { from: dateFormat(d6, "h:MM"), to: dateFormat(d7, "h:MM"), mins: r7 },
                        { from: dateFormat(d7, "h:MM"), to: dateFormat(d8, "h:MM"), mins: r8 },
                    ],
                    totalDelay_go: diff_go,
                    totalDelay_back: diff_back,

                }
            })

        if (this.state.device !== "" && drivers.find(item => item.device == this.state.device) == undefined) {
            data = []
        }
        if (this.state.orderBy === "date") {
            data.sort(function (a, b) {
                return -a.date.getTime() + b.date.getTime();
            });
        }
        if (this.state.orderBy === "delay") {
            data.sort(function (a, b) {
                return -a.totalDelay + b.totalDelay;
            });
        }
        if (this.state.orderBy === "ahead") {
            data.sort(function (a, b) {
                return a.totalDelay - b.totalDelay;
            });
        }
        return data

    }


    resetData() {

        this.setState({
            data: this.paginateData()
        });

    }

    calculateContentHeight() {
        var headerHeight = document.getElementById("header").clientHeight;
        var windowHeight = document.getElementsByTagName("body")[0].clientHeight;
        var filtersHeight = document.getElementById("filters").clientHeight;
        var contentHeight = windowHeight - headerHeight - 2;
        var tableBodyHeight = contentHeight - filtersHeight - 2;
        document.getElementById("content").style.height = contentHeight + 'px';
        document.getElementById("table-body").style.height = tableBodyHeight + 'px';

    }

    handleAliasFilterChange(event) {

        this.setState({ device: event.target.value });
    }


    onClickLogin() {
        let user = document.getElementById("user").value;
        let password = document.getElementById("password").value;
        if (user === "root" && password === "root") {
            this.setState({ login: false });
        } else {
            this.setState({ error: true });
        }
    }

    checkRememberPassword() {
        this.setState({ checked: !this.state.checked });
    }

    menuItemClick(tabNumber) {
        if (tabNumber === 3) {
            this.setState({ login: true, activeTab: 1, error: false });
        } else {
            this.setState({ activeTab: tabNumber });
        }

    }

    render() {
        const getRows = () => this.state.data.map((row, index) => {
            var date = row.date;

            // Hours part from the timestamp

            var day = date.getUTCDate();
            // Minutes part from the timestamp
            var month = date.getUTCMonth() + 1;
            // Seconds part from the timestamp
            var year = date.getFullYear();

            // Will display time in 10:30:23 format
            var formattedTime = day + '/' + month + '/' + year;
            
            if(this.state.route === "Todas"){
                var itemRoute = routes[Math.floor(Math.random() * (routes.length - 2 + 1)) + 1];
            }else{
                let state = this.state;
                var itemRoute = routes.filter(function (route) {
                    return (route.name === state.route);
                })[0];
            }

            return <div key={index} className="col xl12 l12 m12 s12 element no-padding">
                <div className="col xl10 l10 m10 s10  no-padding">
                    <div className="col xl12 l12 m12 s12  title-row">
                        <span className="driver left">{row.driverName}</span>
                        <span className="alias left">{row.device}</span>
                        <span className="route right">{itemRoute.name}</span>
                    </div>
                    <div className="col xl12 l12 m12 s12  no-padding way-row">
                        <div className="col xl1 l1 m1 s1">
                            <span className="way">IDA</span>
                        </div>
                        <div className="col xl2 l2 m2 s2">
                            <span className="start-time">{row.stops_go[0]["from"]}</span>
                            <span className="start-station">{itemRoute.stops[0]}</span>
                        </div>
                        <div className="col xl5 l5 m5 s5">
                            <div className="stops">
                                <div className="stop first">
                                    <span className="circle check">
                                        <img src="assets/images/check-symbol.png" alt="check"/>
                                    </span>
                                    <span className={row.stops_go[0]["mins"]>0?"number delay":"number ahead"}>{row.stops_go[0]["mins"]}</span>
                                </div>
                                <div className="stop">
                                    <span className="circle check">
                                        <img src="assets/images/check-symbol.png" alt="check"/>
                                    </span>
                                    <span className={row.stops_go[1]["mins"]>0?"number delay":"number ahead"}>{row.stops_go[1]["mins"]}</span>
                                </div>
                                <div className="stop">
                                    <span className="circle check">
                                        <img src="assets/images/check-symbol.png" alt="check"/>
                                    </span>
                                    <span className={row.stops_go[2]["mins"]>0?"number delay":"number ahead"}>{row.stops_go[2]["mins"]}</span>
                                </div>
                                <div className="stop last">
                                    <span className="circle check">
                                        <img src="assets/images/check-symbol.png" alt="check"/>
                                    </span>
                                    <span className={row.stops_go[3]["mins"]>0?"number delay":"number ahead"}>{row.stops_go[3]["mins"]}</span>
                                </div>
                            </div>
                        </div>
                        <div className="col xl2 l2 m2 s2">
                            <span className="start-time">{row.stops_go[0]["to"]}</span>
                            <span className="start-station">{itemRoute.stops[itemRoute.stops.length - 1]}</span>
                        </div>
                        <div className="col xl2 l2 m2 s2">
                            <span className="title-delays"><img src="assets/images/circular-clock.png" alt="clock"/> Total Retrasos</span>
                            <span className="delay">03 min</span>
                        </div>
                    </div>
                    <div className="col xl12 l12 m12 s12  no-padding  way-row return">
                        <div className="col xl1 l1 m1 s1">
                            <span className="way">VUELTA</span>
                        </div>
                        <div className="col xl2 l2 m2 s2">
                            <span className="start-time">{row.stops_back[0]["from"]}</span>
                            <span className="start-station">{itemRoute.stops[itemRoute.stops.length - 1]}</span>
                        </div>
                        <div className="col xl5 l5 m5 s5">
                            <div className="stops">
                                <div className="stop first">
                                    <span className="circle check">
                                        <img src="assets/images/check-symbol.png" alt="check"/>
                                    </span>
                                    <span className={row.stops_back[0]["mins"]>0?"number delay":"number ahead"}>{row.stops_back[0]["mins"]}</span>
                                </div>
                                <div className="stop">
                                    <span className="circle check">
                                        <img src="assets/images/check-symbol.png" alt="check"/>
                                    </span>
                                    <span className={row.stops_back[1]["mins"]>0?"number delay":"number ahead"}>{row.stops_back[1]["mins"]}</span>
                                </div>
                                <div className="stop">
                                    <span className="circle check">
                                        <img src="assets/images/check-symbol.png" alt="check"/>
                                    </span>
                                    <span className={row.stops_back[2]["mins"]>0?"number delay":"number ahead"}>{row.stops_back[2]["mins"]}</span>
                                </div>
                                <div className="stop last">
                                    <span className="circle">
                                        &nbsp;
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div className="col xl2 l2 m2 s2">
                            <span className="start-time">{row.stops_back[3]["to"]}</span>
                            <span className="start-station">{itemRoute.stops[0]}</span>
                        </div>
                        <div className="col xl2 l2 m2 s2">
                            <span className="title-delays"><img src="assets/images/circular-clock.png" alt="clock"/> Total Retrasos</span>
                            <span className="delay">03 min</span>
                        </div>
                    </div>
                </div>
                <div className="col xl2 l2 m2 s2  no-padding totals">
                    <div className="date">{dateFormat(row.date, "dd/mmm/yyyy").toString()}</div>
                    <a className="exportPDFItem" href="./assets/downloads/PDF-tracking.pdf" target="_blank">
                        <img src="assets/images/printer.png" alt="Exportar PDF" />
                    </a>
                    <div className="total-title">Total por pagar</div>
                    <div className="price">$50.03</div>
                    <button className="appelations">Ver apelaciones</button>
                </div>
                <div className="col xl12 l12 m12 s12  no-padding">
                    <span className="details">
                        Ver detalles <img src="assets/images/arrow-details.png" alt="arrow-details"/>
                    </span>
                </div>
            </div>
        })

        return <div className="main-container">
            {this.state.login ? <div className="login-container">
                <div className="login-bg"></div>
                <div className="container-fluid">
                    <div className="row no-margin">
                        <div className="col xl6 l6 m6 s6 full-height title-content">
                            <div className="col xl8 l8 m8 s8 partial-width-content">
                                <h1>Controla y mira en <span className="title-strong">tiempo real</span> las rutas</h1>
                                <p>Ahora tienes todo el control de rutas de transportes, si eres una cooperativa puedes gestionar todos tus reportes,
                                ver tiempos, retrazos un muchas cosas más...</p>
                            </div>
                            <img src="assets/images/map-points.png" alt="points" />
                        </div>
                        <div className="col xl6 l6 m6 s6">
                            <div className="full-height login">
                                <h2>Inicia sesión</h2>
                                {this.state.error ? <div className="error">
                                    <img src="assets/images/warning.png" alt="" />
                                    Tu email o contraseÃ±a son incorrectos, intenta de nuevo
                            </div> : null}
                                <input type="text" placeholder="Correo electrónico" id="user" className={this.state.error ? "user error" : "user"} />
                                <input type="password" placeholder="Contraseña" id="password" className={this.state.error ? "password error" : "password"} />
                                <div className="buttons-section">
                                    <div className="col xl7 l7 m7 s7" onClick={this.checkRememberPassword}>
                                        <input type="checkbox" className="filled-in" id="remember-password" checked={this.state.checked} />
                                        <label for="remember-password">Recordar mi contraseña</label>
                                    </div>
                                    <div className="col xl5 l5 m5 s5">
                                        <button className="send-button" onClick={this.onClickLogin}>Iniciar sesión</button>
                                        <a className="forgot-password" href="">Olvidaste tu contraseña?</a>
                                    </div>
                                </div>
                                <div className="by-salmon">
                                    Made with <img src="assets/images/love.png" alt="love" /> by <span className="strong">S4lmon</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> : <div className="dashboard-container">
                    <div className="container-fluid">
                        <div className="row no-margin">
                            <div className="col xl2 l2 m2 s2">
                                <div className="full-height menu">
                                    <img src="assets/images/avatar.png" alt="avatar" className="avatar" />
                                    <h3>Compañía de Transporte Urbano Trans San Carlos S.A.</h3>
                                    <ul>
                                        <li className={this.state.activeTab === 1 ? "tab active" : "tab"} target={1} onClick={() => this.menuItemClick(1)}>
                                            <img src={this.state.activeTab === 1 ? "assets/images/statistics-blue.png" : "assets/images/statistics.png"} alt="icon" />
                                            <span>Seguimiento</span>
                                        </li>
                                        <li className={this.state.activeTab === 2 ? "tab active" : "tab"} target={2} onClick={() => this.menuItemClick(2)}>
                                            <img src={this.state.activeTab === 2 ? "assets/images/settings-blue.png" : "assets/images/settings.png"} alt="icon" />
                                            <span>Configuraciones</span>
                                        </li>
                                        <li className={this.state.activeTab === 3 ? "tab active" : "tab"} target={3} onClick={() => this.menuItemClick(3)}>
                                            <img src={this.state.activeTab === 3 ? "assets/images/power-blue.png" : "assets/images/power.png"} alt="icon" />
                                            <span>Salir</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            {this.state.activeTab == 1 ? <div className="col xl10 l10 m10 s10  no-padding">
                                <div className="content-wrapper">
                                    <div className="header" id="header">
                                        <div className="col xl8 l8 m8 s8">
                                            <h2>Ruta</h2>
                                            <h1 onClick={this.openRoutesModal}>{this.state.route} <img src="assets/images/arrow-big.png" alt="arrow"/></h1> 
                                            <div className={this.state.isOpenedFilterRoutes ? "routesFilter active" : "routesFilter"} id="routesFilter">
                                                <ul>
                                                    {routes.map((route, key) =>
                                                        <li id={"route-"+key} key={key} className={this.state.route === route.name?"active routeSelector":"routeSelector"} onClick={()=>this.setsetRouteFilter(key)}>{route.name}</li>
                                                    )}
                                                </ul>
                                            </div>
                                        </div>
                                        <div className="col xl4 l4 m4 s4 right-align">
                                            <div className="export-buttons">
                                                <a className="exportPDF" href="./assets/downloads/PDF-tracking.pdf" target="_blank">
                                                    <img src="assets/images/printer.png" alt="Exportar PDF" />
                                                </a>
                                                <a className="exportExcel" href="./assets/downloads/rutas.xlsx">
                                                    Exportar
                                            <img src="assets/images/excel.png" alt="Exportar Excel" />
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col xl12 l12 m12 s12 no-padding">
                                        <div className="content" id="content" >
                                            <div className="col xl12 l12 m12 s12 no-padding">
                                                <div className="filters" id="filters">
                                                    <div className="col xl6 l6 m6 s6 no-padding">
                                                        <input className="aliasFilter" type="text" placeholder="Escribe aquí el alias"
                                                            value={this.state.device} onChange={this.handleAliasFilterChange} onKeyPress={this.handleAliasFilterKeyPress}
                                                        />
                                                    </div>
                                                    <div className="col xl6 l6 m6 s6 no-padding">
                                                        <div className="filter-container order">
                                                            <span className="label">Ordenar por:</span>
                                                            <div className="orderFilter filter">
                                                                Fecha
                                                        <img src="assets/images/arrow.png" alt="arrow" />
                                                            </div>
                                                        </div>
                                                        <div className="filter-container date">
                                                            <span className="label">Fecha</span>
                                                            <div className="dateFilter filter" onClick={this.toggleDateFilter}>
                                                                Hoy
                                                        <span>
                                                                    <img src="assets/images/triangle.png" alt="triangle" />
                                                                </span>
                                                            </div>
                                                            <div className={this.state.isOpenedFilterDate ? "dateFilterContent active" : "dateFilterContent"} id="dateFilterContent">
                                                                <ul>
                                                                    <li id="today" className="active dateSelector" onClick={() => this.setDateFilter("today")}>Hoy</li>
                                                                    <li id="lastWeek" className="dateSelector" onClick={() => this.setDateFilter("lastWeek")}>La semana pasada</li>
                                                                    <li id="lastMonth" className="dateSelector" onClick={() => this.setDateFilter("lastMonth")}>El mes pasado</li>
                                                                </ul>
                                                                <div className="personalized">
                                                                    <img src="assets/images/calendar.png" alt="calendar" />
                                                                    Personalizado
                                                                    <span>
                                                                        <input type="date" className="datepicker-from" id="datepicker-from" /> - <input type="date" className="datepicker-to" id="datepicker-to" />
                                                                    </span>
                                                                </div>
                                                                <button className="apply" onClick={this.dateHandleAliasFilterKeyPress}>Aplicar</button>

                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col xl12 l12 m12 s12 no-padding">
                                                <div className="table-body" id="table-body">
                                                    {getRows()}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> : this.state.activeTab == 2 ? <div className="col xl10 l10 m10 s10  no-padding">
                                <div className="content-wrapper">
                                    <div className="header" id="header">
                                        <div className="col xl12 l12 m12 s12 left-align">
                                            <h1>Configuraciones</h1>
                                        </div>
                                    </div>
                                    <div className="col xl12 l12 m12 s12 no-padding">
                                        <div className="content" id="content" >
                                            <div className="row">
                                                <div className='configuracion-container col xl12 l12 m12 s12 no-padding'>
                                                    <div className='box-config'>
                                                        <h3>Cambiar Contraseña</h3>
                                                        <div className='col xl3 l4 m4 left-align login'>
                                                            <span>Contraseña actual </span>
                                                            <input type="password" placeholder="Password actual" id="password" className="inputs-genetal" />
                                                        </div>
                                                        <div className='col x1 l1 m1'>
                                                            <img src="assets/images/line.png" alt="" />
                                                        </div>
                                                        <div className='col xl3 l3 m3 s3  left-align login'>
                                                            <span>Nueva contraseña </span>
                                                            <input type="password" placeholder="Password actual" id="password" className="inputs-genetal" />
                                                        </div>
                                                        <div className='col xl3 l3 m3 s3  left-align login'>
                                                            <span>Repetir nueva contraseña </span>
                                                            <input type="password" placeholder="Password actual" id="password" className="inputs-genetal" />
                                                        </div>
                                                        <div className='row'>
                                                            <div className='col x12 l12 m12'>
                                                                <hr className="lines" />
                                                                <h3>Notificaciones</h3>
                                                            </div>
                                                            <div className="col xl12 l12 m12 s12 buttons-section left-align">
                                                                <input type="checkbox" className="filled-in" id="remember-password" checked="checked" />
                                                                <label >Enviarme un email cuando existan rutas retrazadas</label>
                                                            </div>
                                                            <div className="col xl12 l12 m12 s12 buttons-section left-align">
                                                                <input type="checkbox" className="filled-in" id="remember-password" checked="checked" />
                                                                <label >Enviarme un reporte semanal de mis rutas</label>
                                                            </div>
                                                            <div className="col xl12 l12 m12 s12 buttons-section left-align">
                                                                <input type="checkbox" className="filled-in" id="remember-password" checked="checked" />
                                                                <label >Enviarme un email cuando existan rutas retrazadas</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> : null}
                        </div>
                    </div>
                </div>}
        </div>
    }
}

export default Dashboard;