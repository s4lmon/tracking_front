import com.typesafe.sbt.packager.docker._


name := """tracking_front"""
organization := "com.s4lmon"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.11.11"

libraryDependencies += filters
libraryDependencies += "org.scalatestplus.play" %% "scalatestplus-play" % "2.0.0" % Test

// Adds additional packages into Twirl
//TwirlKeys.templateImports += "com.s4lmon.controllers._"

// Adds additional packages into conf/routes
// play.sbt.routes.RoutesKeys.routesImport += "com.s4lmon.binders._"
enablePlugins(JavaAppPackaging)

dockerBaseImage := "openjdk:latest"

dockerCommands ++= Seq(
  // setting the run script executable
  ExecCmd("RUN",
    "apk add --no-cache curl && mkdir -p /opt &&  curl -sL https://yarnpkg.com/latest.tar.gz | tar xz -C /opt &&  mv /opt/dist /opt/yarn && ln -s /opt/yarn/bin/yarn /usr/local/bin &&  apk del --purge curl")
    //,ExecCmd("ENTRYPOINT","bash")
  
)

dockerExposedPorts := Seq(9000)