const path = require('path');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var webpack = require('webpack');

module.exports = {

  entry: {
    dashboard: "./app/js/dashboard"

  },


  output: {


    path: path.join(__dirname, "public/javascripts"),
    filename: "[name].bundle.js"

  },
  module: {
    loaders: [
      {
        test: /\.js$/,
        loader: 'babel-loader',
        query: {
          presets: ['react', 'es2015', 'stage-2']
        }
      },
      {
        test: /\.scss$/,
        loaders: ['style-loader', 'css-loader', 'sass-loader']
      },
      {
        test: /\.scss$/,
        loader: ExtractTextPlugin.extract('css-loader!sass-loader')
      },
      {
        test: /\.(png|jpg|svg)$/,
        loader: 'file-loader',
        query: {
          name: '../images/[name].bundle.[ext]'
        }
      }
    ]
  },

  plugins: [
    new webpack.ProvidePlugin({
      $: "jquery",
      jQuery: "jquery",
      "window.jQuery": "jquery"

    }),
    new ExtractTextPlugin({ filename: '../stylesheets/[name].bundle.css', disable: false, allChunks: true }),

  ]
}